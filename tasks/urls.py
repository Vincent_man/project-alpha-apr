from django.urls import path
from tasks.views import CreateTask, ListMyTasks

urlpatterns = [
    path("create/", CreateTask, name="create_task"),
    path("mine/", ListMyTasks, name="show_my_tasks"),
]
