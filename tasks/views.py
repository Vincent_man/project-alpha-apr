from django.shortcuts import render, redirect
from tasks.forms import CreateTaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def CreateTask(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTaskForm()
    context = {"create_da_task": form}
    return render(request, "tasks/create_task.html", context)


@login_required
def ListMyTasks(request):
    list = Task.objects.filter(assignee=request.user)
    context = {"my_tasks": list}
    return render(request, "tasks/my_tasks.html", context)
