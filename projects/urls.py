from django.urls import path
from projects.views import ListProjects, ProjectDetail, CreateProject

urlpatterns = [
    path("", ListProjects, name="list_projects"),
    path("<int:id>", ProjectDetail, name="show_project"),
    path("create/", CreateProject, name="create_project"),
]
