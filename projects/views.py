from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from projects.forms import CreateProjectForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def ListProjects(request):
    list = Project.objects.filter(owner=request.user)
    context = {"list_projects": list}
    return render(request, "projects/list_projects.html", context)


@login_required
def ProjectDetail(request, id):
    details = get_object_or_404(Project, id=id)
    context = {"details": details}

    return render(request, "projects/project_details.html", context)


@login_required
def CreateProject(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()

    context = {"create_da_project": form}
    return render(request, "projects/create_project.html", context)
